<?php

system('clear');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/**
 * Indicadores de Cobranza - Customer Services
 * 
 * Se utiliza para adquirir los datos correspondientes a las facturas de los clientes
 * del mes en curso tanto para los Clientes de Venezuela y Puerto Rico.
 * 
 * Se obtiene los registros de ambas bases de datos y se ingresan en los drives
 * correspondientes para que el equipo de data realice su proceso de indicadores.
 * 
 * @author  In. Luis Campos
 * @author campos.luis19@gmail.com
 * 
 */

 